import {AsyncStorage} from 'react-native'
import {getMetricMetaInfo, timeToString} from './helpers'

export const CALENDAR_STORAGE_KEY = 'UdaciFitness:calendar'

function getRandomNumber(max) {
    return Math.floor(Math.random() * max) + 0
}

function setDummyData() {
    console.log("setDummyData called")
    const {run, bike, swim, sleep, eat} = getMetricMetaInfo()

    let dummyData = {}
    const timestamp = Date.now()

    for (let i = -183; i < 0; i++) {
        const time = timestamp + i * 24 * 60 * 60 * 1000
        const strTime = timeToString(time)
        dummyData[strTime] = getRandomNumber(3) % 2 === 0
            ? {
                run: getRandomNumber(run.max),
                bike: getRandomNumber(bike.max),
                swim: getRandomNumber(swim.max),
                sleep: getRandomNumber(sleep.max),
                eat: getRandomNumber(eat.max),
            }
            : null
    }

    AsyncStorage.setItem(CALENDAR_STORAGE_KEY, JSON.stringify(dummyData))

    console.log("dummyData fom setDummyData", dummyData)
    return dummyData
}

function setMissingDates(dates) {
    console.log("setMissingDates called with ", dates)
    const length = Object.keys(dates).length
    const timestamp = Date.now()

    for (let i = -183; i < 0; i++) {
        const time = timestamp + i * 24 * 60 * 60 * 1000
        const strTime = timeToString(time)

        if (typeof dates[strTime] === 'undefined') {
            dates[strTime] = null
        }
    }

    console.log("dates after setting:",dates)

    return dates
}

export function formatCalendarResults(results) {
    console.log("formatCalendarResults", results)
    console.log("results===null? ", results===null)
    return results === null
        ? setDummyData()
        : setMissingDates(JSON.parse(results))
}
