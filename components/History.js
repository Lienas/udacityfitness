import React from 'react';
import {Text, View} from "react-native";
import {connect} from 'react-redux'
import {addEntry, receiveEntries} from "../actions";
import {getDailyReminderValue, timeToString} from "../utils/helpers";
import {fetchCalenderResults} from "../utils/api";
import {Agenda as UdaciFitnessCalendar} from 'react-native-calendars'


class History extends React.Component {

    componentDidMount() {
        console.log("history: componentDidMount called")
        const {dispatch} = this.props
        console.log("props:", this.props)
        fetchCalenderResults()
            .then((entries) => dispatch(receiveEntries(entries)))
            .then(({entries}) => {
                if (!entries[timeToString()]) {
                    dispatch(addEntry({
                        [timeToString()]: getDailyReminderValue()
                    }))
                }
            })
            .then(() => this.setState(() => ({ready: true})))
    }

    renderItem = ({today, ...metrics}, formattedDate, key) => {
        console.log("renderItem was called")
        return (
            <View>
                {today
                    ? <Text>today: {JSON.stringify(today)}</Text>
                    : <Text>metrics: {JSON.stringify(metrics)}</Text>
                }
            </View>
        )
    }

    renderEmptyDate = (formattedDate) => {
        console.log("renderEmpty date param:", formattedDate)
        return <View>
                    <Text>No Data for this day</Text>
               </View>
    }

    render() {
        const {entries} = this.props
        console.log("history: render called")

        return (
            <UdaciFitnessCalendar
                items={entries}
                renderItem={this.renderItem}
                renderEmptyDay={this.renderEmptyDate}
            />
        );
    }
}

function mapStateToProps(entries) {
    return {
        entries
    }
}

export default connect(mapStateToProps)(History);
