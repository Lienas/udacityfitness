import React from 'react';
import {Text, TouchableOpacity} from 'react-native'
import {StyleSheet} from "react-native";
import {purple} from "../utils/colors";

function TextButton({onClick, children,style={}}) {
    return (
      <TouchableOpacity onPress={onClick}>
          <Text style={[styles.reset,style]}>{children}</Text>
      </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    reset: {
        textAlign: 'center',
        color: purple
    }
})

export default TextButton;
